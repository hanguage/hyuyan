function doJS编() {
    // All future JS编 words can see local variables in here, so don't use
    // any local variable here. They can *see* variables & functions out side
    // this function too, that's normal.
    compiling = "JS编"; // it's true and a clue of compiling a JS编 word.
    newname = nexttoken();
    if (isReDef(newname)) panic("reDef " + newname + "\n"); // don't use tick(newname), it's wrong.
    push(nextstring("JS。"));
    if (tos().flag) {
        // _me is the JS编 word object itself.
        eval(
            'newxt=function(_me){ /* ' + newname + ' */\n' +
            pop().str + '\n}' // the ending "\n}" allows // comment at the end
        );
    } else {
        panic("Error! expecting 'JS。'.\n");
        reset();
    }
}

"JS。",
function() {
    if (compiling != "JS编") {
        panic("Error! 'JS。' to a none JS编 word.\n");
        return
    };
    current_word_list().push(new Word([newname, newxt]));
    last().vid = current;
    last().wid = current_word_list().length - 1;
    last().type = 'JS编';
    last().help = newhelp;
    vm.execute("默认私有");
    last().private = Boolean(pop());
    wordhash[last().name] = last();
    compiling = false;
}

"予以入册",
    function() {
        if (!(newname = pop())) panic("(create) what?\n", tib.length - ntib > 100);
        if (isReDef(newname)) type("reDef " + newname + "\n"); // 若用 tick(newname) 就錯了
        current_word_list().push(new Word([newname, function() {}]));
        last().vid = current; // vocabulary ID
        last().wid = current_word_list().length - 1; // word ID
        last().type = "colon-create";
        vm.execute("默认私有"); // use the original execute() to avoid warning
        last().private = Boolean(pop());
    }

"编",
    function() {
        newname = nexttoken();
        push(newname);
        X变 = 0;
        X集 = [0];
        execute("予以入册"); // execute("(create)");
        compiling = "编";
        tick('编').stackwas = stack.slice(0); // Should not be changed, ';' will check.
        last().type = "colon";
        last().cfa = here;
        last().help = newhelp;
        last().xt = colonxt = function() {
            rstack.push(ip);
            inner(this.cfa);
        }
    }

"。",
    function() {
        if (!vm.数组全交(tick('编').stackwas, stack)) { // vm.g.isSameArray词组全交
            panic("Stack changed during colon definition, it must be a mistake!\n",
                "error");
            words[current].pop();
        } else {
            comma(RET);
        }
        compiling = false;
        X集 = [0];
        X变 = 0;
        wordhash[last().name] = last(); // execute('reveal'); 
    }


JS编 (create)   ( "name" -- ) \ Create a JS编 word that has a dummy xt, not added into wordhash{} yet
                if(!(newname=pop())) panic("(create) what?\n", tib.length-ntib>100);
                if(isReDef(newname)) type("reDef "+newname+"\n"); // 若用 tick(newname) 就錯了
                current_word_list().push(new Word([newname,function(){}]));
                last().vid = current; // vocabulary ID
                last().wid = current_word_list().length-1; // word ID
                last().type = "colon-create";
                vm.execute("privacy"); // use the original execute() to avoid warning
                last().private = Boolean(pop());
                JS。

JS编 reveal     ( -- ) \ Add the last word into wordhash
                wordhash[last().name]=last() JS。

JS编 colon-word ( -- ) \ Decorate the last() as a colon word.
                // last().type = "colon";
                last().cfa = here;
                last().xt = colonxt;
                JS。 private

: compile       ( -- ) \ Compile the next word at dictionary[ip] to dictionary[here].
                r> dup @ , 1+ >r ; compile-only 
JS编 doVar      push(ip); ip=rstack.pop(); JS。 compile-only private

: create        ( <name> -- ) \ Create a new word. The new word is a variable by default.
                BL word (create) reveal colon-word compile doVar ;
: variable      ( <string> -- ) \ Create a variable.
                create 0 , [ char push(function(){last().type='colon-variable'}) jsEvalNo , ] ;
    